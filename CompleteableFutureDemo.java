import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompleteableFutureDemo {

    static void tryRunAsync() throws InterruptedException, ExecutionException {
        System.out.println("Demo : tryRunAsyc()");
        System.out.println("Before call ...");

        CompletableFuture<Void> cf = CompletableFuture.runAsync(() -> {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("runAsync(Runnable) : Run void function ... ");
        });

        System.out.println("Wait for 2secs sleep ... ");
        cf.get();

        System.out.println("----");
        Thread.sleep(1000);
    }

    static void trySupplyAsync() throws InterruptedException, ExecutionException {
        System.out.println("Demo : trySupplyAsyc()");
        System.out.println("Before call ...");
        CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return "supplyAsync(Runnable) : return the result ... ";
        });

        System.out.println("Wait for 2secs sleep ... ");
        System.out.println(cf.get());

        System.out.println("----");
        Thread.sleep(1000);
    }

    static void tryCallbacks() throws InterruptedException, ExecutionException {
        System.out.println("Demo : tryCallbacks()");

        System.out.println("thenApply(fn) ...");
        CompletableFuture<String> thenApplyRes = CompletableFuture.supplyAsync(() -> {
            return "Teh manis";
        }).thenApply(it -> {
            return it + " kalaupun gak pake gula";
        }).thenApply(it -> {
            return it + " tetep manis.";
        });

        System.out.println(thenApplyRes.get());


        System.out.println("thenAccept(void fn) ...");
        CompletableFuture.supplyAsync(() -> {
            return "Teh tawar";
        }).thenAccept(it -> {
            System.out.println(it + " dikasih gula jadi manis");
        });

        System.out.println("thenRun(void fn) ...");
        CompletableFuture<Void> thenRunRes = thenApplyRes.thenRun(() -> {
            System.out.println("previous task done.");
        });

        System.out.println("isinya --> " + thenRunRes.get());


        System.out.println("thenApplyAsync(fn) ...");
        CompletableFuture<String> thenApplyAsyncRes = CompletableFuture.supplyAsync(() -> {
            return "Teh manis";
        }).thenApplyAsync(it -> {
            return it + " kalaupun gak pake gula";
        }).thenApplyAsync(it -> {
            return it + " tetep manis.";
        });

        System.out.println(thenApplyAsyncRes.get());

        System.out.println("----");
        Thread.sleep(1000);
    }

    static void tryComposeCombine() throws InterruptedException, ExecutionException {
        System.out.println("Demo : tryComposeCombine()");

        System.out.println("[compose] Before call ...");
        CompletableFuture<String> cfcp = CompletableFuture.supplyAsync(() -> "Aku sayang")
        .thenCompose(it -> CompletableFuture.supplyAsync(() -> it + " kamu"));
        System.out.println(cfcp.get());

        System.out.println("[combine] Before call ...");
        CompletableFuture<String> cfcb = CompletableFuture.supplyAsync(() -> "Aku sayang")
        .thenCombine(CompletableFuture.supplyAsync(() -> " kamu"), (a, b) -> a + b);
        System.out.println(cfcb.get());

        System.out.println("----");
        Thread.sleep(1000);
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        tryRunAsync();
        trySupplyAsync();
        tryCallbacks();
        tryComposeCombine();
    }

}