import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class FutureDemo {

    static void tryFuture() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        System.out.println("Demo : tryFuture() ");
        System.out.println("Before call ...");

        Callable<String> callable = () -> {
            Thread.sleep(2000);
            return "Callable returns.";
        };

        Future<String> future = executorService.submit(callable);

        System.out.println("Wait 2 sec ...");

        System.out.println(future.get());

        System.out.println("------");
        Thread.sleep(1000);
        executorService.shutdown();
    }

    static void tryIsDone() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        System.out.println("Demo : tryIsDone() ");
        System.out.println("Before call ...");

        Callable<String> callable = () -> {
            Thread.sleep(2000);
            return "Callable returns.";
        };

        Future<String> future = executorService.submit(callable);

        while(!future.isDone()) {
            System.out.println("Wait until done ...");
            Thread.sleep(200);
        }

        System.out.println(future.get());

        System.out.println("------");
        Thread.sleep(1000);
        executorService.shutdown();
    }

    static void tryTimeOut(int limitMilis) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        System.out.println("Demo : tryTimeOut() ");
        System.out.println("Before call ...");

        Callable<String> callable = () -> {
            Thread.sleep(2000);
            return "Callable returns.";
        };

        Future<String> future = executorService.submit(callable);

        try {
            System.out.println(future.get(limitMilis, TimeUnit.MILLISECONDS));
        } catch (Exception e) {
            System.out.println("Time's out ...");
        }

        System.out.println("------");
        Thread.sleep(1000);
        executorService.shutdown();
    }

    static void tryInvokeAll() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        System.out.println("Demo : tryInvokeAll() ");
        System.out.println("Before call ...");

        Callable<String> callable1 = () -> {
            Thread.sleep(2000);
            return "[1] Callable returns.";
        };

        Callable<String> callable2 = () -> {
            Thread.sleep(1000);
            return "[2] Callable returns.";
        };

        Callable<String> callable3= () -> {
            Thread.sleep(5000);
            return "[3] Callable returns.";
        };

        List<Callable<String>> tasksList = Arrays.asList(callable1, callable2, callable3);
        List<Future<String>> futures = executorService.invokeAll(tasksList);

        for (Future<String> future : futures) {
            System.out.println(future.get());
        }

        System.out.println("------");
        Thread.sleep(1000);
        executorService.shutdown();
    }

    static void tryInvokeAny() throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        System.out.println("Demo : tryInvokeAny() ");
        System.out.println("Before call ...");

        Callable<String> callable1 = () -> {
            Thread.sleep(2000);
            return "[1] Callable returns.";
        };

        Callable<String> callable2 = () -> {
            Thread.sleep(1000);
            return "[2] Callable returns.";
        };

        Callable<String> callable3= () -> {
            Thread.sleep(5000);
            return "[3] Callable returns.";
        };

        List<Callable<String>> tasksList = Arrays.asList(callable1, callable2, callable3);

        System.out.println(executorService.invokeAny(tasksList));

        System.out.println("------");
        Thread.sleep(1000);
        executorService.shutdown();
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        tryFuture();
        tryIsDone();
        tryTimeOut(1000);
        tryInvokeAll();
        tryInvokeAny();
    }

}